﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

    /*
     * Project BLoB Test2D
     * Author Yoshua Neuhard
     * Date 12/12/2016
     */

    public GameObject player;

    public GameObject shot;
    public Transform shotSpawn;
    public float firerate;

    private Rigidbody2D playerbody;

    private float lp = 0.0f;
    private float nextFire = 0.0f;
	
    void Start()
    {
        lp = 100f;
        playerbody = GetComponent<Rigidbody2D>();
    }


	void Update ()
    {
        if (Input.GetButton("Fire1") && Time.time > nextFire)
        {
            nextFire = Time.time + firerate;
            GameObject shots = Instantiate(shot, shotSpawn.position, shotSpawn.rotation) as GameObject;
        }
    }

    void FixedUpdate()
    {         

        // movement TEMPORARY, change for mobile
        var x = Input.GetAxis("Horizontal") * 20.0f;
        var y = Input.GetAxis("Vertical") * 20.0f;

        if (Input.GetKey(KeyCode.W))
        {
            playerbody.MoveRotation(playerbody.rotation + 100 * Time.fixedDeltaTime);
        }
        else if (Input.GetKey(KeyCode.S))
        {
            playerbody.MoveRotation(playerbody.rotation - 100 * Time.fixedDeltaTime);
        }

        playerbody.AddForce(new Vector2(x, y));
    }

    // collision test (change to recieve dmg)!!
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Boundary" || other.tag == "Player" || other.tag == "enemy" || other.tag == "ally")
        {
            return;
        }
        else if (other.tag == "bullet")
        {
            lp -= 10f;
            if (lp <= 0f)
            {
                Destroy(player);
            }
            print("player hp: "+lp);
            Destroy(other.gameObject);
        }
    }
}

