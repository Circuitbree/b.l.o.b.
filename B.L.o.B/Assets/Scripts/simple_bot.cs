﻿using UnityEngine;
using System.Collections;

public class simple_bot : MonoBehaviour {

    /*
     * Project BLoB Test2D
     * Author Yoshua Neuhard
     * Date 12/12/2016
     */

    public GameObject enemy;

    public Transform shotSpawn;
    public float firerate;
    public GameObject shot;

    private Rigidbody2D enemybody;
    private GameObject player;
    private Rigidbody2D playerbody;


    private float lp = 0.0f;
    private float nextFire = 0.0f;
    private Vector2 vectoplayer;

    void Start()
    {
        lp = 100f;
        player = GameObject.FindGameObjectWithTag("Player");
        playerbody = player.GetComponent<Rigidbody2D>();
        enemybody = GetComponent<Rigidbody2D>();
    }

    void Update () {

        if (Time.time > nextFire)
        {
            nextFire = Time.time + firerate;
            GameObject shots = Instantiate(shot, shotSpawn.position, shotSpawn.rotation) as GameObject;
        }

    }

    void FixedUpdate()
    {

        // test movement based on random and always look towards player
        vectoplayer = playerbody.position - enemybody.position;
        float angle = Mathf.Acos((vectoplayer.y) / (Mathf.Sqrt(vectoplayer.x * vectoplayer.x + vectoplayer.y * vectoplayer.y))) * 180 / Mathf.PI;
        enemybody.rotation = (vectoplayer.x > 0) ? -angle : angle;

        enemybody.AddForce(new Vector2(Random.Range(-20f, 20f), Random.Range(-20f, 20f)));
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Boundary" || other.tag == "Player" || other.tag == "enemy" || other.tag == "ally")
        {
            return;
        }
        else if (other.tag == "bullet")
        {
            lp -= 10f;
            if (lp <= 0f)
            {
                Destroy(enemy);
            }
            print("enemy hp: " + lp);
            Destroy(other.gameObject);
        }
    }

    
}
