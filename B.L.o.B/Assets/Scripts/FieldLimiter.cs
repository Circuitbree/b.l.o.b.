﻿using UnityEngine;
using System.Collections;

public class FieldLimiter : MonoBehaviour {

    /*
    * Project BLoB Test2D
    * Author Yoshua Neuhard
    * Date 12/12/2016
    */

    void OnTriggerExit2D(Collider2D other)
    {
        Destroy(gameObject);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Map")
        {
            Destroy(gameObject);
        }
        
    }
}
