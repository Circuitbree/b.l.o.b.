﻿using UnityEngine;
using System.Collections;

public class MoveShot : MonoBehaviour {

    /*
    * Project BLoB Test2D
    * Author Yoshua Neuhard
    * Date 12/12/2016
    */

    private Rigidbody2D shotBody;

    public float speed;

    void Start()
    {
        shotBody = GetComponent<Rigidbody2D>();
        shotBody.velocity = transform.up * speed;
    }
}
